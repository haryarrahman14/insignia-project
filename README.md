# Insignia Mini Project

![Preview with frame](./public/assets/preview-frame.png)
![Preview with frame](./public/assets/preview.png)

## Deployment :

https://insignia-project.vercel.app/

## Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

Don't forget to `npm install` before running the apps
