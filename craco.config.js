const path = require('path');
module.exports = {
  webpack: {
    alias: {
      components: path.resolve(__dirname, 'src/components'),
      hooks: path.resolve(__dirname, 'src/hooks'),
      pages: path.resolve(__dirname, 'src/pages'),
      assets: path.resolve(__dirname, 'public/assets/img'),
      utils: path.resolve(__dirname, 'src/utils'),
      consts: path.resolve(__dirname, 'src/consts'),
      client: path.resolve(__dirname, 'src/client'),
      src: path.resolve(__dirname, 'src'),
      types: path.resolve(__dirname, 'src/types')
    }
  }
};
