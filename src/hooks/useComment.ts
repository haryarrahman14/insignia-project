import { UseQueryOptions, useMutation, useQuery } from '@tanstack/react-query';

import {
  deleteComment,
  getCommentByPost,
  getCommentByUser,
  getCommentDetail,
  getComments,
  postComment
} from 'client/commentClient';

export const useGetComments = (
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getComments(params);
  };

  return useQuery<any>(['comments', params], fetchData, options);
};

export const useGetDetailComment = (
  id: string | number,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getCommentDetail(id);
  };

  return useQuery<any>(['comments', id], fetchData, options);
};

export const usePostComment = () => {
  return useMutation((body: Record<string, unknown>) => postComment(body));
};

export const useDeleteComment = () => {
  return useMutation((id: string | number) => deleteComment(id));
};

export const useGetCommentByUser = (
  userId: string | number,
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getCommentByUser(userId, params);
  };

  return useQuery<any>(
    ['comments-by-user', userId, params],
    fetchData,
    options
  );
};

export const useGetCommentByPost = (
  postId: string | number,
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getCommentByPost(postId, params);
  };

  return useQuery<any>(
    ['comments-by-post', postId, params],
    fetchData,
    options
  );
};
