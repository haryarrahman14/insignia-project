import { useGetDetailUser, useGetUsers } from './useUser';

const useGlobalUser = () => {
  const { data } = useGetUsers(
    {
      page: 0,
      limit: 5
    },
    {
      select: (data: any) => data?.data?.data
    }
  );
  const userData = data?.[0];
  const { data: fullUserData } = useGetDetailUser(userData?.id, {
    select: (data: any) => data?.data
  });

  return {
    user: fullUserData
  };
};

export default useGlobalUser;
