import { UseQueryOptions, useMutation, useQuery } from '@tanstack/react-query';

import {
  deletePost,
  getPostByTag,
  getPostByUser,
  getPostDetail,
  getPosts,
  postPost,
  putPost
} from 'client/postClient';

export const useGetPosts = (
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getPosts(params);
  };

  return useQuery<any>(['posts', params], fetchData, options);
};

export const useGetDetailPost = (
  id: string | number,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getPostDetail(id);
  };

  return useQuery<any>(['posts', id], fetchData, options);
};

export const usePostPost = () => {
  return useMutation((body: Record<string, unknown>) => postPost(body));
};

export const useDeletePost = () => {
  return useMutation((id: string | number) => deletePost(id));
};

export const useEditPost = (id: string | number) => {
  return useMutation((body: Record<string, unknown>) => putPost(id, body));
};

export const useGetPostByUser = (
  userId: string | number,
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getPostByUser(userId, params);
  };

  return useQuery<any>(['posts-by-user', userId, params], fetchData, options);
};

export const useGetPostByTag = (
  tagId: string | number,
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getPostByTag(tagId, params);
  };

  return useQuery<any>(['posts-by-tag', tagId, params], fetchData, options);
};
