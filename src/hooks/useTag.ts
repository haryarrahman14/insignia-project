import { UseQueryOptions, useQuery } from '@tanstack/react-query';

import { getTags } from 'client/tagClient';

export const useGetTags = (
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getTags(params);
  };

  return useQuery<any>(['tags', params], fetchData, options);
};
