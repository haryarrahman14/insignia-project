import { UseQueryOptions, useMutation, useQuery } from '@tanstack/react-query';

import {
  deleteUser,
  getUserDetail,
  getUsers,
  postUser,
  putUser
} from 'client/userClient';

export const useGetUsers = (
  params: Record<string, unknown>,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getUsers(params);
  };

  return useQuery<any>(['users', params], fetchData, options);
};

export const useGetDetailUser = (
  id: string | number,
  options?: UseQueryOptions
) => {
  const fetchData = () => {
    return getUserDetail(id);
  };

  return useQuery<any>(['users', id], fetchData, options);
};

export const usePostUser = () => {
  return useMutation((body: Record<string, unknown>) => postUser(body));
};

export const useDeleteUser = () => {
  return useMutation((id: string | number) => deleteUser(id));
};

export const useEditUser = (id: string | number) => {
  return useMutation((body: Record<string, unknown>) => putUser(id, body));
};
