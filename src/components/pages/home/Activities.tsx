import dayjs from 'dayjs';

import { useGetComments } from 'hooks/useComment';
import { useGetDetailPost } from 'hooks/usePost';

const Activity = ({ postId, comment }: { postId: string; comment: any }) => {
  const { data: post } = useGetDetailPost(postId, {
    select: (data: any) => data?.data
  });

  return (
    <div className="grid grid-cols-3 col-span-3 gap-[10px] p-[5px] rounded-lg border-[2px] border-solid group hover:border-gray-800 border-transparent hover:scale-105 hover:z-10 hover:shadow-md transition-all cursor-pointer">
      <div className="col-span-1 h-[80px] overflow-hidden flex items-center rounded-md border-[2px] border-solid group-hover:!border-transparent border-gray-800 transition-all">
        <img
          src={post?.image}
          className="min-h-full h-auto bg-cover bg-no-repeat bg-center opacity-90 grow"
        />
      </div>

      <div className="col-span-2 flex flex-col justify-between">
        <p className="font-pulp text-[1.2rem] text-gray-800 truncate">
          {comment?.owner?.firstName}
        </p>
        <p className="text-[1rem] font-medium text-gray-800 truncate">
          {comment?.message}
        </p>
        <p className="text-[0.6rem] font-bold text-gray-800 truncate">
          {dayjs(comment?.publishDate)?.format('DD-MM-YYYY')}
        </p>
      </div>
    </div>
  );
};

const Activities = () => {
  const { data: comments } = useGetComments(
    {
      page: 0,
      limit: 5
    },
    {
      select: (data: any) => data?.data?.data
    }
  );

  return (
    <div className="w-full grid grid-cols-3 p-[30px] gap-[20px] bg-[#FCFCFC] shadow-sm rounded-xl">
      <p className="col-span-1 font-pulp capitalize text-[1.5rem] leading-[20px]">
        Activities
      </p>
      <p className="col-span-2 text-right text-[0.8rem] cursor-pointer hover:text-gray-500 leading-[20px]">
        View Timeline / Filter Activities
      </p>

      <div className="col-span-3 h-[2px] bg-gray-500" />

      <div className="col-span-3 flex flex-col gap-[10px]">
        {comments?.map((comment: any, index: number) => (
          <Activity key={index} postId={comment?.post} comment={comment} />
        ))}
      </div>

      <div className="col-span-3 h-[1px] bg-gray-500" />
    </div>
  );
};

export default Activities;
