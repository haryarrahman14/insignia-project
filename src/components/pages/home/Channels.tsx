import { tags } from 'consts/tags';

import Button from 'components/shared/Button';

const Channels = () => {
  return (
    <div className="w-full grid grid-cols-3 p-[30px] gap-[20px] bg-[#FCFCFC] shadow-sm rounded-xl">
      <p className="col-span-3 font-pulp capitalize text-[1.5rem] leading-[20px]">
        All Channels
      </p>

      <div className="col-span-3 h-[2px] bg-gray-500" />

      {tags?.map((tag: string, index: number) => (
        <Button key={index} variant="secondary">
          {tag}
        </Button>
      ))}
    </div>
  );
};

export default Channels;
