/* eslint-disable @typescript-eslint/no-explicit-any */
import { useGetPostByTag } from 'hooks/usePost';

const ContentLarge = ({
  img,
  name,
  text,
  likes
}: {
  img: string;
  name: string;
  text: string;
  likes: string;
}) => {
  return (
    <div className="relative lg:col-span-2 lg:min-w-full min-w-[80%] max-h-[210px] overflow-hidden flex items-center rounded-lg lg:hover:scale-105 hover:z-10 hover:shadow-md transition-all bg-gray-800 cursor-pointer border-[2px] border-solid border-gray-800">
      <img
        src={img}
        className="w-full h-auto bg-cover bg-no-repeat bg-center opacity-90"
      />

      <div className="absolute flex flex-col left-[10px] right-[10px] bottom-[10px]">
        <p className="text-gray-200 font-medium text-[1.5rem]">{name}</p>

        <div className="grid grid-cols-3 gap-[10px]">
          <p className="col-span-2 text-gray-200 font-medium text-[1rem] truncate">
            {text}
          </p>

          <p className="col-span-1 text-gray-200 text-right font-medium text-[1rem] truncate">
            {likes} Likes
          </p>
        </div>
      </div>
    </div>
  );
};

const ContentMini = ({
  img,
  name,
  likes
}: {
  img: string;
  name: string;
  likes: string;
}) => {
  return (
    <div className="relative max-h-[100px] overflow-hidden flex items-center rounded-lg hover:scale-105 hover:z-10 hover:shadow-md transition-all bg-gray-800 cursor-pointer border-[2px] border-solid border-gray-800">
      <img
        src={img}
        className="w-full h-auto bg-cover bg-no-repeat bg-center opacity-90"
      />

      <div className="absolute grid grid-cols-2 gap-[10px] left-[10px] right-[10px] bottom-[10px]">
        <p className="col-span-1 text-gray-200 font-medium text-[1rem] truncate">
          {name}
        </p>

        <p className="col-span-1 text-gray-200 text-right font-medium text-[1rem] truncate">
          {likes} Likes
        </p>
      </div>
    </div>
  );
};

const Contents = ({ tag }: { tag: string }) => {
  const { data }: any = useGetPostByTag(
    tag,
    {
      page: 0,
      limit: 5
    },
    {
      select: (data: any) => data?.data?.data
    }
  );

  return (
    <div className="w-full grid grid-cols-3 p-[30px] gap-[12px] bg-[#FCFCFC] shadow-sm rounded-xl">
      <p className="lg:col-span-1 col-span-2 font-pulp capitalize text-[1.5rem] leading-[20px]">
        {tag}
      </p>
      <p className="col-span-1 text-right text-[1rem] cursor-pointer hover:text-gray-500 leading-[20px]">
        Show All
      </p>

      <div className="hidden lg:grid col-span-3 grid-rows-3 gap-[10px]">
        <div className="grid row-span-2 grid-cols-3 gap-[10px]">
          {/* Main Content */}
          <ContentLarge
            img={data?.[0]?.image}
            name={data?.[0]?.owner?.firstName}
            text={data?.[0]?.text}
            likes={data?.[0]?.likes}
          />

          {/* Beside Main Content */}
          <div className="hidden lg:grid grid-rows-2 gap-[10px]">
            {data
              ?.slice(1, 3)
              ?.map((content: any, index: number) => (
                <ContentMini
                  key={index}
                  img={content?.image}
                  name={content?.owner?.firstName}
                  likes={content?.likes}
                />
              ))}
          </div>
        </div>

        {/* Under Main Content */}
        <div className="grid row-span-1 grid-cols-3 gap-[10px]">
          {data
            ?.slice(3, 5)
            ?.map((content: any, index: number) => (
              <ContentMini
                key={index}
                img={content?.image}
                name={content?.owner?.firstName}
                likes={content?.likes}
              />
            ))}

          <div className="max-h-[100px] overflow-hidden flex flex-row justify-center items-center p-[10px] rounded-lg hover:scale-105 hover:z-10 hover:shadow-md transition-all border-[2px] border-solid border-gray-800 cursor-pointer">
            <svg
              className="w-6 h-6 text-gray-800 "
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 20"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M10 5.757v8.486M5.757 10h8.486M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
              />
            </svg>
            <p className="ml-[8px] font-pulp leading-[0px]">
              Share your Stories
            </p>
          </div>
        </div>
      </div>

      <div className="col-span-3 lg:hidden flex flex-row overflow-scroll gap-[20px]">
        {data?.map((content: any, index: number) => (
          <ContentLarge
            key={index}
            img={content?.image}
            name={content?.owner?.firstName}
            text={content?.text}
            likes={content?.likes}
          />
        ))}

        <div className="min-w-[80%] h-full overflow-hidden flex flex-row justify-center items-center p-[10px] rounded-lg lg:hover:scale-105 hover:z-10 hover:shadow-md transition-all border-[2px] border-solid border-gray-800 cursor-pointer">
          <svg
            className="w-6 h-6 text-gray-800 "
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 20 20"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M10 5.757v8.486M5.757 10h8.486M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
            />
          </svg>
          <p className="ml-[8px] font-pulp leading-[0px]">Share your Stories</p>
        </div>
      </div>
    </div>
  );
};

export default Contents;
