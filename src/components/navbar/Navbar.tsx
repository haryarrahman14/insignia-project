import LargeNavbar from './LargeNavbar';
import MobileNavbar from './MobileNavbar';

const Navbar = () => {
  return (
    <div className="bg-[#F4F5F9]">
      {/* Large */}
      <LargeNavbar />

      {/* Mobile */}
      <MobileNavbar />
    </div>
  );
};

export default Navbar;
