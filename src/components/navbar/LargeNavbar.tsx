/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState } from 'react';
import { useLocation } from 'react-router-dom';

import { tags } from 'consts/tags';
import useGlobalUser from 'hooks/useGlobalUser';

import Button from 'components/shared/Button';
import Dropdown from 'components/shared/Dropdown';
import Search from 'components/shared/Search';

const LargeNavbar = () => {
  const { pathname } = useLocation();

  const { user } = useGlobalUser();

  const [isOpen, setIsOpen] = useState(false);

  const onSearch = (searchValue: string) => {
    console.log({ searchValue });
  };

  return (
    <div className="hidden md:flex flex-col max-w-[1200px] mx-auto py-[20px]">
      <div className="flex flex-col mx-[20px] p-[30px] gap-[20px] bg-[#FCFCFC] shadow-sm rounded-xl">
        <div className="flex flex-row items-center gap-[10px] grow">
          <img src="assets/logo-large.svg" height={36} />
          <Search onSearch={onSearch} />
          <Button variant="secondary">
            <div className="flex flex-row items-center gap-[10px]">
              <svg
                className="w-[16px] h-[16px] text-gray-800"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 16 16"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M8 12V1m0 0L4 5m4-4 4 4m3 5v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-3"
                />
              </svg>
              <p>Upload</p>
            </div>
          </Button>
          <Dropdown>
            <Dropdown.Header onClick={() => setIsOpen(!isOpen)}>
              <div className="flex flex-row items-center gap-[8px]">
                <img
                  className="rounded-lg"
                  src={user?.picture}
                  height={30}
                  width={30}
                />
                <div className="flex flex-col">
                  <p className="font-medium text-sm text-left">
                    {user?.firstName}
                  </p>
                  <p className="font-medium text-xs text-left">
                    {user?.lastName}
                  </p>
                </div>
              </div>
            </Dropdown.Header>

            <Dropdown.Options
              position="right"
              isOpen={isOpen}
              customClass="w-[150px]"
            >
              <Dropdown.Option>
                <div className="flex flex-row justify-between items-center">
                  <p>My Profile</p>
                  <svg
                    className="w-[16px] h-[16px] text-gray-800 "
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    viewBox="0 0 20 18"
                  >
                    <path d="M6.5 9a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9Zm-1.391 7.361.707-3.535a3 3 0 0 1 .82-1.533L7.929 10H5a5.006 5.006 0 0 0-5 5v2a1 1 0 0 0 1 1h4.259a2.975 2.975 0 0 1-.15-1.639ZM8.05 17.95a1 1 0 0 1-.981-1.2l.708-3.536a1 1 0 0 1 .274-.511l6.363-6.364a3.007 3.007 0 0 1 4.243 0 3.007 3.007 0 0 1 0 4.243l-6.365 6.363a1 1 0 0 1-.511.274l-3.536.708a1.07 1.07 0 0 1-.195.023Z" />
                  </svg>
                </div>
              </Dropdown.Option>
              <Dropdown.Option>
                <div className="flex flex-row justify-between items-center">
                  <p>Security</p>
                  <svg
                    className="w-[16px] h-[16px] text-gray-800 "
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    viewBox="0 0 20 19"
                  >
                    <path d="M7.324 9.917A2.479 2.479 0 0 1 7.99 7.7l.71-.71a2.484 2.484 0 0 1 2.222-.688 4.538 4.538 0 1 0-3.6 3.615h.002ZM7.99 18.3a2.5 2.5 0 0 1-.6-2.564A2.5 2.5 0 0 1 6 13.5v-1c.005-.544.19-1.072.526-1.5H5a5.006 5.006 0 0 0-5 5v2a1 1 0 0 0 1 1h7.687l-.697-.7ZM19.5 12h-1.12a4.441 4.441 0 0 0-.579-1.387l.8-.795a.5.5 0 0 0 0-.707l-.707-.707a.5.5 0 0 0-.707 0l-.795.8A4.443 4.443 0 0 0 15 8.62V7.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.12c-.492.113-.96.309-1.387.579l-.795-.795a.5.5 0 0 0-.707 0l-.707.707a.5.5 0 0 0 0 .707l.8.8c-.272.424-.47.891-.584 1.382H8.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1.12c.113.492.309.96.579 1.387l-.795.795a.5.5 0 0 0 0 .707l.707.707a.5.5 0 0 0 .707 0l.8-.8c.424.272.892.47 1.382.584v1.12a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1.12c.492-.113.96-.309 1.387-.579l.795.8a.5.5 0 0 0 .707 0l.707-.707a.5.5 0 0 0 0-.707l-.8-.795c.273-.427.47-.898.584-1.392h1.12a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5ZM14 15.5a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5Z" />
                  </svg>
                </div>
              </Dropdown.Option>
            </Dropdown.Options>
          </Dropdown>
        </div>

        <div className="inline-block items-center mx-[-10px]">
          {tags?.map((tag: string, index: number) => (
            <Button
              key={index}
              variant={
                pathname == `/tag/${tag}` || (tag == 'all' && pathname == '/')
                  ? 'primary'
                  : 'secondary'
              }
              customClass="min-w-[120px] m-[5px] capitalize"
            >
              {tag}
            </Button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default LargeNavbar;
