import { useState } from 'react';

const Search = ({ onSearch }: { onSearch: (value: string) => void }) => {
  const [value, setValue] = useState('');
  return (
    <form
      className="flex grow"
      onSubmit={(e: React.FormEvent<HTMLFormElement>) => (
        e.preventDefault(), onSearch(value)
      )}
    >
      <div className="flex grow relative">
        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
          <svg
            className="w-4 h-4 text-gray-500 "
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 20 20"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
            />
          </svg>
        </div>
        <input
          type="search"
          id="default-search"
          className="block w-full p-4 pl-10 text-sm bg-transparent text-gray-900 border border-gray-300 rounded-lg focus:ring-blue-500 focus:border-blue-500"
          placeholder="Find..."
          value={value}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            setValue(event?.target?.value)
          }
        />
        <button
          type="submit"
          className="text-white absolute right-2.5 bottom-2.5 bg-[#4361EE] hover:bg-blue-800 font-medium rounded-lg text-sm px-4 py-2"
        >
          Search
        </button>
      </div>
    </form>
  );
};

export default Search;
