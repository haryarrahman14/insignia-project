interface IDropdown {
  children: JSX.Element | JSX.Element[] | string;
}

interface IHeader {
  onClick?: () => void;
  customClass?: string;
  children: JSX.Element | JSX.Element[] | string;
}

interface IOptions {
  customClass?: string;
  position?: 'left' | 'right';
  isOpen?: boolean;
  children: JSX.Element | JSX.Element[] | string;
}

interface IOption {
  customClass?: string;
  children: JSX.Element | JSX.Element[] | string;
}

const Dropdown = ({ children }: IDropdown) => {
  return <div className="relative">{children}</div>;
};

const Header = ({ customClass = '', onClick, children }: IHeader) => {
  return (
    <div
      className={`cursor-pointer ${customClass}`}
      onClick={onClick && onClick}
    >
      {children}
    </div>
  );
};

const Options = ({
  customClass = '',
  position = 'left',
  isOpen = false,
  children
}: IOptions) => {
  const positions = {
    left: 'left-0 text-left',
    right: 'right-0 text-right'
  };

  return (
    <div
      id="dropdown"
      className={`absolute mt-[10px] z-10 bg-white divide-y divide-gray-100 rounded-lg shadow ${positions[position]} ${customClass}`}
    >
      {isOpen && <ul className="py-2 text-sm text-gray-700">{children}</ul>}
    </div>
  );
};

const Option = ({ customClass = '', children }: IOption) => {
  return (
    <li
      className={`block px-4 py-2 hover:bg-gray-100 cursor-pointer ${customClass}`}
    >
      {children}
    </li>
  );
};

Dropdown.Header = Header;
Dropdown.Options = Options;
Dropdown.Option = Option;

export default Dropdown;
