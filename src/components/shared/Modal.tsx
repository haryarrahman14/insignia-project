interface IModal {
  onClose?: () => void;
  children: JSX.Element | JSX.Element[] | string;
}

const Modal = ({ onClose, children }: IModal) => {
  return (
    <div className="absolute w-screen h-screen z-10 left-0 right-0 top-0 bottom-0 bg-[rgba(23,25,35,.5)]">
      <div className="absolute top-[50%] left-[50%] translate-y-[-50%] translate-x-[-50%]">
        <div className="mx-auto bg-[#FCFCFC] rounded-[20px] p-[20px]">
          {children}
        </div>
        <div
          className="cursor-pointer mx-auto mt-[30px] flex items-center justify-center w-[36px] h-[36px] rounded-lg hover:bg-white hover:text-[rgba(23,25,35,.5)] text-white"
          onClick={onClose}
        >
          <svg
            className="w-[20px] h-[20px]"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 14"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
            />
          </svg>
        </div>
      </div>
    </div>
  );
};

export default Modal;
