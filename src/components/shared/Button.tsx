interface IButton {
  variant?: 'primary' | 'secondary' | 'transparent';
  customClass?: string;
  onClick?: () => void;
  children: JSX.Element | JSX.Element[] | string;
}

const Button = ({
  variant = 'primary',
  customClass = '',
  onClick,
  children
}: IButton) => {
  const variants = {
    primary: 'text-white bg-[#4361EE] hover:bg-blue-800',
    secondary: 'text-gray-900 bg-gray-100 hover:bg-gray-200',
    transparent: 'text-gray-900 bg-white border border-gray-300'
  };

  return (
    <button
      type="button"
      className={`${variants[variant]} font-medium rounded-lg text-sm px-5 py-2.5 focus:outline-none ${customClass}`}
      onClick={onClick && onClick}
    >
      {children}
    </button>
  );
};

export default Button;
