import axios from 'axios';

const client = axios.create({
  baseURL: 'https://dummyapi.io/data/v1/',
  headers: {
    Accept: 'application/json',
    'app-id': '65183e92bce3c7a0979614cc'
  }
});

client.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

client.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default client;
