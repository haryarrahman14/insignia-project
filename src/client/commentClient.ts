import client from './apiClient';

export const getComments = (params: Record<string, unknown>) => {
  return client.get('comment', { params });
};

export const getCommentDetail = (id: string | number) => {
  return client.get(`comment/${id}`);
};

export const postComment = (body: Record<string, unknown>) => {
  return client.post('comment/create', body);
};

export const deleteComment = (id: string | number) => {
  return client.delete(`comment/${id}`);
};

export const getCommentByUser = (
  id: string | number,
  params: Record<string, unknown>
) => {
  return client.get(`user/${id}/comment`, { params });
};

export const getCommentByPost = (
  id: string | number,
  params: Record<string, unknown>
) => {
  return client.get(`post/${id}/comment`, { params });
};
