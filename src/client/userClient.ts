import client from './apiClient';

export const getUsers = (params: Record<string, unknown>) => {
  return client.get('user', { params });
};

export const getUserDetail = (id: string | number) => {
  return client.get(`user/${id}`);
};

export const postUser = (body: Record<string, unknown>) => {
  return client.post('user/create', body);
};

export const putUser = (id: string | number, body: Record<string, unknown>) => {
  return client.put(`user/${id}`, body);
};

export const deleteUser = (id: string | number) => {
  return client.delete(`user/${id}`);
};
