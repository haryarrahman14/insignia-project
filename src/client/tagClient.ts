import client from './apiClient';

export const getTags = (params: Record<string, unknown>) => {
  return client.get('tag', { params });
};
