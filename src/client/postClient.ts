import client from './apiClient';

export const getPosts = (params: Record<string, unknown>) => {
  return client.get('post', { params });
};

export const getPostDetail = (id: string | number) => {
  return client.get(`post/${id}`);
};

export const postPost = (body: Record<string, unknown>) => {
  return client.post('post/create', body);
};

export const putPost = (id: string | number, body: Record<string, unknown>) => {
  return client.put(`post/${id}`, body);
};

export const deletePost = (id: string | number) => {
  return client.delete(`post/${id}`);
};

export const getPostByUser = (
  id: string | number,
  params: Record<string, unknown>
) => {
  return client.get(`user/${id}/post`, { params });
};

export const getPostByTag = (
  id: string | number,
  params: Record<string, unknown>
) => {
  return client.get(`tag/${id}/post`, { params });
};
