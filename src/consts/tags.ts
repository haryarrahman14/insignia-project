export const tags: string[] = [
  'all',
  'person',
  'human',
  'dog',
  'water',
  'pool',
  'ocean',
  'nature'
];
