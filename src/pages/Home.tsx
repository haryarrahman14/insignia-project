import { tags } from 'consts/tags';

import Activities from 'components/pages/home/Activities';
import Channels from 'components/pages/home/Channels';
import Contents from 'components/pages/home/Contents';

const Home = () => {
  return (
    <div className="max-w-[1200px] mx-auto">
      <div className="grid grid-cols-3 gap-[24px] mx-[20px] overflow-hidden">
        <div className="lg:col-span-2 col-span-3 grid gap-[24px]">
          {tags
            ?.slice(1, 4)
            ?.map((tag: string, index: number) => (
              <Contents key={index} tag={tag} />
            ))}
        </div>

        <div className="lg:col-span-1 col-span-3 flex flex-col gap-[20px]">
          <Activities />
          <Channels />
        </div>
      </div>
    </div>
  );
};

export default Home;
