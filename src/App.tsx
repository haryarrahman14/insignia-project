import { BrowserRouter as Router } from 'react-router-dom';

import Footer from 'components/footer/Footer';
import Navbar from 'components/navbar/Navbar';

import Routes from './routes';

function App() {
  return (
    <Router>
      <div className="bg-[#F4F5F9]">
        <Navbar />
        <Routes />
        <Footer />
      </div>
    </Router>
  );
}

export default App;
